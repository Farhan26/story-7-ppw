from django.apps import AppConfig


class AccrodionConfig(AppConfig):
    name = 'accrodion'
