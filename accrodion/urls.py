from django.urls import path
from . import views

app_name = 'accrodion'

urlpatterns = [
    path('', views.accord, name='accord'),
]
